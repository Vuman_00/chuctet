package com.deginer.fithou.vumanhduc.chuctet.Adapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.deginer.fithou.vumanhduc.chuctet.Models.Sms;

import java.util.ArrayList;

/**
 * Created by VuManhDuc on 25/03/2017.
 */

public class SmsAdapter extends ArrayAdapter<Sms>{

    public SmsAdapter(Context context, ArrayList<Sms> smses) {

        super(context, android.R.layout.simple_list_item_1,smses);
    }
}
