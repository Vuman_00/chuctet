package com.deginer.fithou.vumanhduc.chuctet.Models;

/**
 * Created by VuManhDuc on 24/03/2017.
 */

public class Title {
    private  int id;
    private  String title;

    public Title(){}

    public Title(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
