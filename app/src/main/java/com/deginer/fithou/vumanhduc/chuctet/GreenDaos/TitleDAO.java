package com.deginer.fithou.vumanhduc.chuctet.GreenDaos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.deginer.fithou.vumanhduc.chuctet.Models.Title;

import java.util.ArrayList;

/**
 * Created by VuManhDuc on 24/03/2017.
 */

public class TitleDAO {
    public static final String TABLE_NAME = "tb_cat";
    public static final String KEY_ID = "id";
    public static final String KEY_TITLE = "name";

    SQLiteDatabase db;
    TitleHelper helper;

    public TitleDAO(Context context) {
        helper = new TitleHelper(context);
        helper.creatDB();
        db = helper.openDB();

    }

    public ArrayList<Title> getList(){
        ArrayList<Title> notes = new ArrayList<>();

        // Lenh lay ra bang tb_cat
        String selectQuery = "SELECT"+KEY_ID+","+KEY_TITLE+"FROM"+TABLE_NAME;

        Cursor cursor = db.rawQuery(selectQuery,null);

        if(cursor.moveToFirst()){
            do {
                Title title = new Title();
                title.setId(cursor.getInt(0));
                title.setTitle(cursor.getString(1));
                ////
                notes.add(title);
            }while (cursor.moveToNext());
        }
        cursor.close();
        ///Tra ve mot danh sach TITLE
        return notes;


    }
}
