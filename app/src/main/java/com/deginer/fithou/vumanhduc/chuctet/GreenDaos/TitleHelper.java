package com.deginer.fithou.vumanhduc.chuctet.GreenDaos;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by VuManhDuc on 24/03/2017.
 */

public class TitleHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "db_sms_love";

    private String path;
    private Context context;


    public TitleHelper(Context context) {
        super(context, DB_NAME, null,1);
        this.context = context;
        this.path = context.getFilesDir().getParent()+"/database/"+DB_NAME;
        //super(context, name, factory, version);
    }

    public TitleHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }
    public boolean checkDB(){
        SQLiteDatabase db=null;
        try {
            db = SQLiteDatabase.openDatabase(path,null,SQLiteDatabase.OPEN_READWRITE);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (db!= null){
            db.close();
            return true;
        }
        return false;
    }

    public SQLiteDatabase openDB(){
        return SQLiteDatabase.openDatabase(path,null,SQLiteDatabase.OPEN_READWRITE);
    }
    public  void creatDB(){
        boolean check = checkDB();
        if(check){
            Log.d("Ket Noi","Máy đã có Database");
        }else {
            Log.d("Ket Noi","Máy chưa có database");
            this.getWritableDatabase();
            copyDB();
        }

    }

    private void copyDB() {
        try {
            InputStream is = context.getAssets().open(DB_NAME);
            OutputStream os = new FileOutputStream(path);
            byte[] buffer = new byte[1024];
            int lenght;
            while((lenght=is.read(buffer))>0)   {
                os.write(buffer,0,lenght);
            }
            os.flush();
            os.close();
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
