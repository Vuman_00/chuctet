package com.deginer.fithou.vumanhduc.chuctet.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deginer.fithou.vumanhduc.chuctet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListSmsFragment extends Fragment {


    public ListSmsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_sms, container, false);
    }

}
