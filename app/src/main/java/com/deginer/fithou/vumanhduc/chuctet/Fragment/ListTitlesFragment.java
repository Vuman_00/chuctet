package com.deginer.fithou.vumanhduc.chuctet.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.deginer.fithou.vumanhduc.chuctet.MainActivity;
import com.deginer.fithou.vumanhduc.chuctet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListTitlesFragment extends Fragment {


    public ListTitlesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((MainActivity)getActivity()).getSupportActionBar().setTitle("Lựa chọn chủ đề");
        return inflater.inflate(R.layout.fragment_list_titles, container, false);
    }

}
