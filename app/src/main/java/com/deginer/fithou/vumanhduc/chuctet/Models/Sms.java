package com.deginer.fithou.vumanhduc.chuctet.Models;

/**
 * Created by VuManhDuc on 24/03/2017.
 */

public class Sms {
    private int bookmart;
    private int id;
    private int type;
    private String content;

    public Sms() {
    }

    public int getBookmart() {
        return bookmart;
    }

    public void setBookmart(int bookmart) {
        this.bookmart = bookmart;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content;
    }
}

